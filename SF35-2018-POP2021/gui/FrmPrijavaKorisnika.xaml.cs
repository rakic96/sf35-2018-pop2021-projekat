﻿using SF35_2018_POP2021.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF35_2018_POP2021.gui
{
    /// <summary>
    /// Interaction logic for FrmPrijavaKorisnika.xaml
    /// </summary>
    public partial class FrmPrijavaKorisnika : Window
    {
        public FrmPrijavaKorisnika()
        {
            InitializeComponent();
        }

        private void btnPrijava_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;

            string korIme = tbKorIme.Text;
            string lozinka = pbLozinka.Password;
            Korisnik k = prijavaKorisnika(korIme, lozinka);

            Podaci.prijavljenKorisnik = k;
 
        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public Korisnik prijavaKorisnika(string korisnicko, string lozinka)
        {

            Korisnik k = null;
            foreach (Administrator admin in Podaci.lstAdmini)
                if (admin.Jmbg == korisnicko && admin.Lozinka == lozinka)
                {
                    k = admin;
                    Podaci.prijavljenAdmin = admin;
                    Podaci.prijavljenLekar = null;
                    Podaci.prijavljenPacijent = null;
                }

            foreach (Lekar lekar in Podaci.lstLekari)
                if (lekar.Jmbg == korisnicko && lekar.Lozinka == lozinka)
                {
                    k = lekar;
                    Podaci.prijavljenLekar = lekar;
                    Podaci.prijavljenAdmin = null;
                    Podaci.prijavljenPacijent = null;
                }

            foreach (Pacijent pacijent in Podaci.lstPacijenti)
                if (pacijent.Jmbg == korisnicko && pacijent.Lozinka == lozinka)
                {
                    k = pacijent;
                    Podaci.prijavljenPacijent = pacijent;
                    Podaci.prijavljenAdmin = null;
                    Podaci.prijavljenLekar = null;
                }

            return k;
        }
    }
}
