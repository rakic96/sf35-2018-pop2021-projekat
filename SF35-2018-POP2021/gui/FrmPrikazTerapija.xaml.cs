﻿using SF35_2018_POP2021.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF35_2018_POP2021.gui
{
    /// <summary>
    /// Interaction logic for FrmPrikazTerapija.xaml
    /// </summary>
    public partial class FrmPrikazTerapija : Window
    {
        Pacijent pacijent = null;
        Lekar lekar = null;
        public FrmPrikazTerapija(Pacijent pacijent, Lekar lekar)
        {
            InitializeComponent();

            this.pacijent = pacijent;
            this.lekar = lekar;

            if (pacijent != null)
                lbKartonPacijenta.Content = "Karton pacijenta: " + pacijent.Ime + " " + pacijent.Prezime + "\nJMBG: " + pacijent.Jmbg;

            if (lekar == null)//prijavio se lekar, onemoguciti azuriranje terapija
            {
                btnDodaj.Visibility = Visibility.Hidden;
                btnIzmeni.Visibility = Visibility.Hidden;
                btnBrisi.Visibility = Visibility.Hidden;
            }

            if (pacijent != null)
                for (int i = 0; i < Podaci.lstPacijenti.Count; i++)
                    if (Podaci.lstPacijenti.ElementAt(i).Jmbg.Equals(pacijent.Jmbg))
                        dgPrikazKartona.ItemsSource = Podaci.lstPacijenti.ElementAt(i).LstTerapije;
        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (tbOpis.Text != "")
            {
                int id = Podaci.generisiId_Terapija();
                string opis = tbOpis.Text;
                //public Terapija(int id, string opis, Lekar lekar)
                Terapija terapija = new Terapija(id, opis, lekar, pacijent);

                for (int i = 0; i < Podaci.lstPacijenti.Count; i++)
                    if (Podaci.lstPacijenti.ElementAt(i).Jmbg.Equals(pacijent.Jmbg))
                        Podaci.lstPacijenti.ElementAt(i).LstTerapije.Add(terapija);
                Podaci.lstTerapija.Add(terapija);

                dgPrikazKartona.Items.Refresh();

                tbOpis.Text = "";
            }
            else
            {
                MessageBox.Show("Morate uneti opis!");
            }


        }

        private void btnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            if (tbOpis.Text != "")
            {
                string opis = tbOpis.Text;

                if (dgPrikazKartona.SelectedIndex > -1)
                {
                    Terapija terapija = (Terapija)dgPrikazKartona.SelectedItem;
                    for (int i = 0; i < Podaci.lstPacijenti.Count; i++)
                        if (Podaci.lstPacijenti.ElementAt(i).Jmbg.Equals(pacijent.Jmbg))
                            for (int j = 0; j < Podaci.lstPacijenti.ElementAt(i).LstTerapije.Count; j++)
                                if (Podaci.lstPacijenti.ElementAt(i).LstTerapije.ElementAt(j).Id == terapija.Id)
                                    Podaci.lstPacijenti.ElementAt(i).LstTerapije.ElementAt(j).Opis = opis;

                    dgPrikazKartona.Items.Refresh();

                }
            }
            else
            {
                MessageBox.Show("Morate uneti opis!");
            }
        }

        private void btnBrisi_Click(object sender, RoutedEventArgs e)
        {
            string opis = tbOpis.Text;

            if (dgPrikazKartona.SelectedIndex > -1)
            {
                Terapija terapija = (Terapija)dgPrikazKartona.SelectedItem;

                for (int i = 0; i < Podaci.lstPacijenti.Count; i++)
                    if (Podaci.lstPacijenti.ElementAt(i).Jmbg.Equals(pacijent.Jmbg))
                    {
                        int indeksBrisanje = -1;
                        for (int j = 0; j < Podaci.lstPacijenti.ElementAt(i).LstTerapije.Count; j++)
                            if (Podaci.lstPacijenti.ElementAt(i).LstTerapije.ElementAt(j).Id == terapija.Id)
                                indeksBrisanje = j;

                        if (indeksBrisanje > -1)
                            Podaci.lstPacijenti.ElementAt(i).LstTerapije.RemoveAt(indeksBrisanje);
                    }

                dgPrikazKartona.Items.Refresh();
                tbOpis.Text = "";

            }
        }

        private void dgPrikazKartona_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgPrikazKartona.SelectedIndex > -1)
            {
                Terapija terapija = (Terapija)dgPrikazKartona.SelectedItem;
                tbOpis.Text = terapija.Opis;
            }
        }
    }
}
