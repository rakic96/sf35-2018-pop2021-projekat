﻿using SF35_2018_POP2021.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF35_2018_POP2021.gui
{
    /// <summary>
    /// Interaction logic for FrmUpis_DomZdravlja.xaml
    /// </summary>
    public partial class FrmUpis_DomZdravlja : Window
    {
        string upisIzmena = "upis";
        DomZdravlja domZdravlja = null;//nedefinisan objekat
        public FrmUpis_DomZdravlja(string upisIzmena, DomZdravlja domZdravlja)
        {
            InitializeComponent();
            this.upisIzmena = upisIzmena;
            this.domZdravlja = domZdravlja;

            if (upisIzmena == "izmena")
            {
                this.Title = "Izmena Doma zdravlja";
                btnDodaj.Content = "Izmeni";

                tbNaziv.Text = domZdravlja.Naziv;
                tbUlica.Text = domZdravlja.Ulica;
                tbBroj.Text = domZdravlja.Broj.ToString();
                tbGrad.Text = domZdravlja.Grad;
                tbDrzava.Text = domZdravlja.Drzava;


            }
               
        }

        string validno()
        {

            string tekst = "";
            if (tbNaziv.Text == "")
                tekst += "\nMorate uneti naziv!";
            if (tbUlica.Text == "")
                tekst += "\nMorate uneti ulicu!";
            if (tbBroj.Text == "")
                tekst += "\nMorate uneti broj!";
            if (tbGrad.Text == "")
                tekst += "\nMorate uneti grad!";
            if (tbDrzava.Text == "")
                tekst += "\nMorate uneti drzavu!";
            if (!int.TryParse(tbBroj.Text, out int n))
                tekst += "\nMorate uneti cifre za broj!";

            return tekst;

        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {

            if (validno() == "")
            {

                string naziv = tbNaziv.Text;
                string ulica = tbUlica.Text;
                int broj = Convert.ToInt32(tbBroj.Text);
                string grad = tbGrad.Text;
                string drzava = tbDrzava.Text;

                if (upisIzmena == "upis")
                {
                    int idAdresa = Podaci.generisiId_Adresa();
                    int idDomZdr = Podaci.generisiId_DomZdravlja();

                    Adresa adr = new Adresa(idAdresa, ulica, broj, grad, drzava);
                    Podaci.lstAdresa.Add(adr);

                    DomZdravlja dz = new DomZdravlja(idDomZdr, naziv, adr);
                    Podaci.lstDomoviZdravlja.Add(dz);

                    tbNaziv.Text = "";
                    tbUlica.Text = "";
                    tbBroj.Text = "";
                    tbGrad.Text = "";
                    tbDrzava.Text = "";

                }
                else if (upisIzmena == "izmena")
                {

                    DialogResult = true;//sluzi za proveru iz MainWindowa kad se pozove izmena. U glavnoj formi ce se azurirati tabela nakon sto je ovde potvrdjeno

                    int idDomZdravlja = domZdravlja.Id;
                    for (int i = 0; i < Podaci.lstDomoviZdravlja.Count; i++) //i indeks za pristup elementima liste. Pristupa se listi lstDomoviZdravlja
                    {
                        if (Podaci.lstDomoviZdravlja.ElementAt(i).Id == idDomZdravlja) //ElementAt(i)  element liste na poziciji i
                        {
                            Podaci.lstDomoviZdravlja.ElementAt(i).Naziv = naziv;
                            Podaci.lstDomoviZdravlja.ElementAt(i).Ulica = ulica;
                            Podaci.lstDomoviZdravlja.ElementAt(i).Broj = broj;
                            Podaci.lstDomoviZdravlja.ElementAt(i).Grad = grad;
                            Podaci.lstDomoviZdravlja.ElementAt(i).Drzava = drzava;

                        }
                    }

                }
                this.Close();

            }
            else
            {
                MessageBox.Show(validno(), "Greska prilikom upisa");
            }
        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
