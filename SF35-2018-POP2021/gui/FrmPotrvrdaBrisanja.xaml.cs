﻿using SF35_2018_POP2021.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF35_2018_POP2021.gui
{
    /// <summary>
    /// Interaction logic for FrmPotrvrdaBrisanja.xaml
    /// </summary>
    public partial class FrmPotrvrdaBrisanja : Window
    {
        int idBrisanje = 0;
        Object objekatBrisanje = null;
        public FrmPotrvrdaBrisanja(Object objekatBrisanje)
        {
            InitializeComponent();
            this.objekatBrisanje = objekatBrisanje;
        }

        private void btnNe_Click(object sender, RoutedEventArgs e)
        {
            this.Close();

        }

        private void btnDa_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            if (objekatBrisanje is DomZdravlja)
            {
                //Podaci.lstDomoviZdravlja.Remove((DomZdravlja)objekatBrisanje);
                DomZdravlja dz = (DomZdravlja)objekatBrisanje;
                for (int i = 0; i < Podaci.lstDomoviZdravlja.Count; i++)
                {
                    if (Podaci.lstDomoviZdravlja.ElementAt(i).Id == dz.Id)
                        Podaci.lstDomoviZdravlja.ElementAt(i).obrisiDZ();
                }
            }
            if (objekatBrisanje is Pacijent)
            {
                Pacijent p = (Pacijent)objekatBrisanje;
                for (int i = 0; i < Podaci.lstPacijenti.Count; i++)
                {
                    if (Podaci.lstPacijenti.ElementAt(i).Jmbg == p.Jmbg)
                        Podaci.lstPacijenti.ElementAt(i).obrisiKorisnika();
                }
            }
            if (objekatBrisanje is Lekar)
            {
                Lekar lekar = (Lekar)objekatBrisanje;
                for (int i = 0; i < Podaci.lstLekari.Count; i++)
                {
                    if (Podaci.lstLekari.ElementAt(i).Jmbg == lekar.Jmbg)
                        Podaci.lstLekari.ElementAt(i).obrisiKorisnika();
                }
            }

            this.Close();
        }
    }
}
