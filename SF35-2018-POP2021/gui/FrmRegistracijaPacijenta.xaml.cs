﻿using SF35_2018_POP2021.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF35_2018_POP2021.gui
{
    /// <summary>
    /// Interaction logic for FrmRegistracijaPacijenta.xaml
    /// </summary>
    /// 
    //public Pacijent(string ime, string prezime, string jmbg, string email, Adresa adresa, Pol pol, string lozinka)
            //: base(ime, prezime, jmbg, email, adresa, pol, lozinka)


    public partial class FrmRegistracijaPacijenta : Window
    {
        string upisIzmena = "upis";
        Pacijent pacijent = null;

        public FrmRegistracijaPacijenta(string upisIzmena, Pacijent pacijent)
        {
            InitializeComponent();
            this.upisIzmena = upisIzmena;
            this.pacijent = pacijent;

            if (upisIzmena == "izmena")
            {
                this.Title = "Izmena pacijenta";
                this.btDodaj.Content = "Izmeni";

                tbIme.Text = pacijent.Ime;
                tbPrezime.Text = pacijent.Prezime;
                tbJmbg.Text = pacijent.Jmbg;
                tbEmail.Text = pacijent.Email;

                tbUlica.Text = pacijent.Adresa.Ulica;
                tbBroj.Text = pacijent.Adresa.Broj.ToString();
                tbGrad.Text = pacijent.Adresa.Grad;
                tbDrzava.Text = pacijent.Adresa.Drzava;

                if (pacijent.Pol == Pol.MUSKI)
                    rbMuski.IsChecked = true;
                else
                    rbZenski.IsChecked = true;
                tbLozinka.Text = pacijent.Lozinka;



            }


        }

        string validno()
        {

            string tekst = "";
            if (tbIme.Text == "")
                tekst += "\nMorate uneti ime!";
            if (tbPrezime.Text == "")
                tekst += "\nMorate uneti prezime!";
            if (tbJmbg.Text == "")
                tekst += "\nMorate uneti jmbg!";
            if (tbEmail.Text == "")
                tekst += "\nMorate uneti email!";
            if (tbLozinka.Text == "")
                tekst += "\nMorate uneti lozinku!";

            if (tbUlica.Text == "")
                tekst += "\nMorate uneti ulicu!";
            if (tbBroj.Text == "")
                tekst += "\nMorate uneti broj!";
            if (tbGrad.Text == "")
                tekst += "\nMorate uneti grad!";
            if (tbDrzava.Text == "")
                tekst += "\nMorate uneti drzavu!";
            if (!int.TryParse(tbBroj.Text, out int n))
                tekst += "\nMorate uneti cifre za broj!";

            if (upisIzmena == "upis")
            {
                bool postojiJmbg = false;
                foreach (Pacijent p in Podaci.lstPacijenti)
                    if (p.Jmbg == tbJmbg.Text)
                        postojiJmbg = true;
                foreach (Lekar l in Podaci.lstLekari)
                    if (l.Jmbg == tbJmbg.Text)
                        postojiJmbg = true;
                if (postojiJmbg == true)
                    tekst += "\nUnet Jmbg vec postoji!";
            }


            return tekst;

        }

        private void btDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (validno() == "")
            {
                string ime = tbIme.Text;
                string prezime = tbPrezime.Text;
                string jmbg = tbJmbg.Text;
                string email = tbEmail.Text;
                string ulica = tbUlica.Text;
                int broj = Convert.ToInt32(tbBroj.Text);
                string grad = tbGrad.Text;
                string drzava = tbDrzava.Text;

                Pol polPacijent = Pol.MUSKI;
                if (rbZenski.IsChecked == true)
                    polPacijent = Pol.ZENSKI;

                string lozinka = tbLozinka.Text;



                if (upisIzmena == "upis")
                {
                    //public Adresa(int id, string ulica, int broj, string grad, string drzava)
                    int idAdresa = Podaci.generisiId_Adresa();
                    Adresa adresa = new Adresa(idAdresa, ulica, broj, grad, drzava);
                    Podaci.lstAdresa.Add(adresa);

                    Pacijent pacijent = new Pacijent(ime, prezime, jmbg, email, adresa, polPacijent, lozinka);
                    Podaci.lstPacijenti.Add(pacijent);
                }

                if (upisIzmena == "izmena")
                {
                    DialogResult = true; //19.01.

                    for (int i = 0; i < Podaci.lstPacijenti.Count; i++)
                    {
                        if (Podaci.lstPacijenti.ElementAt(i).Jmbg.Equals(jmbg))
                        {
                            Podaci.lstPacijenti.ElementAt(i).Ime = ime;
                            Podaci.lstPacijenti.ElementAt(i).Prezime = prezime;
                            Podaci.lstPacijenti.ElementAt(i).Email = email;

                            Podaci.lstPacijenti.ElementAt(i).Adresa.Ulica = ulica;
                            Podaci.lstPacijenti.ElementAt(i).Adresa.Broj = broj;
                            Podaci.lstPacijenti.ElementAt(i).Adresa.Grad = grad;
                            Podaci.lstPacijenti.ElementAt(i).Adresa.Drzava = drzava;

                            Podaci.lstPacijenti.ElementAt(i).Pol = polPacijent;
                            Podaci.lstPacijenti.ElementAt(i).Lozinka = lozinka;




                        }
                    }

                }

                this.Close();
            }
            else
            {
                MessageBox.Show(validno(), "Greska prilikom upisa");
            }


        }

        private void btZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        
    }
}
