﻿using SF35_2018_POP2021.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF35_2018_POP2021.gui
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class FrmRegistracijaLekara : Window
    {
        string upisIzmena = "upis";
        Lekar lekar = null;
        public FrmRegistracijaLekara(string upisIzmena, Lekar lekar)
        {
            InitializeComponent();

            this.upisIzmena = upisIzmena;
            this.lekar = lekar;

            //inicijalizuje combo box za domove zdravlja
            foreach (DomZdravlja dz in Podaci.lstDomoviZdravlja)
                cbDomZdravlja.Items.Add(dz.Id + ") " +dz.Naziv);
            if (cbDomZdravlja.Items.Count > 0)
                cbDomZdravlja.SelectedIndex = 0;

            if (upisIzmena == "izmena")
            {
                this.Title = "Izmena lekara";
                this.btDodaj.Content = "Izmeni";

                tbIme.Text = lekar.Ime;
                tbPrezime.Text = lekar.Prezime;
                tbJmbg.Text = lekar.Jmbg;
                tbEmail.Text = lekar.Email;

                tbUlica.Text = lekar.Adresa.Ulica;
                tbBroj.Text = lekar.Adresa.Broj.ToString();
                tbGrad.Text = lekar.Adresa.Grad;
                tbDrzava.Text = lekar.Adresa.Drzava;

                if (lekar.Pol == Pol.MUSKI)
                    rbMuski.IsChecked = true;
                else
                    rbZenski.IsChecked = true;
                tbLozinka.Text = lekar.Lozinka;

                tbJmbg.IsEnabled = false;

                if (lekar.DomZdravlja != null)
                {
                    cbDomZdravlja.Text= lekar.DomZdravlja.Id + ") " + lekar.DomZdravlja.Naziv;
                }

            }
        }

        private void btZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        string validno()
        {

            string tekst = "";
            if (tbIme.Text == "")
                tekst += "\nMorate uneti ime!";
            if (tbPrezime.Text == "")
                tekst += "\nMorate uneti prezime!";
            if (tbJmbg.Text == "")
                tekst += "\nMorate uneti jmbg!";
            if (tbEmail.Text == "")
                tekst += "\nMorate uneti email!";
            if (tbLozinka.Text == "")
                tekst += "\nMorate uneti lozinku!";

            if (tbUlica.Text == "")
                tekst += "\nMorate uneti ulicu!";
            if (tbBroj.Text == "")
                tekst += "\nMorate uneti broj!";
            if (tbGrad.Text == "")
                tekst += "\nMorate uneti grad!";
            if (tbDrzava.Text == "")
                tekst += "\nMorate uneti drzavu!";
            if (!int.TryParse(tbBroj.Text, out int n))
                tekst += "\nMorate uneti cifre za broj!";

            if (upisIzmena == "upis")
            {
                bool postojiJmbg = false;
                foreach (Pacijent p in Podaci.lstPacijenti)
                    if (p.Jmbg == tbJmbg.Text)
                        postojiJmbg = true;
                foreach (Lekar l in Podaci.lstLekari)
                    if (l.Jmbg == tbJmbg.Text)
                        postojiJmbg = true;
                if (postojiJmbg == true)
                    tekst += "\nUnet Jmbg vec postoji!";
            }

            return tekst;

        }

        private void btDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (validno() == "")
            {
                string ime = tbIme.Text;
                string prezime = tbPrezime.Text;
                string jmbg = tbJmbg.Text;
                string email = tbEmail.Text;
                string ulica = tbUlica.Text;
                int broj = Convert.ToInt32(tbBroj.Text);
                string grad = tbGrad.Text;
                string drzava = tbDrzava.Text;

                Pol polPacijent = Pol.MUSKI;
                if (rbZenski.IsChecked == true)
                    polPacijent = Pol.ZENSKI;


                string lozinka = tbLozinka.Text;


                DomZdravlja dzLekara = null;//objekat bez instanciranja, nedefinisana vrednost. AKo ostane null prosledice se tako kao parametar konstr klase Lekar
                if (cbDomZdravlja.SelectedIndex >= 0)//da ne dodje do greske ako ne postoje elementi
                {
                    string domZdravljaId = cbDomZdravlja.SelectedItem.ToString(); //preuzme tekst iz cb koji sadrzi id)naziv
                    string[] nizEl = domZdravljaId.Split(')');//niz sadrzi 2 elementa: id, naziv
                    int idDZ = Convert.ToInt32(nizEl[0]); //dom zdravlja treba pronaci u listi domova zdr na osnovu id

                    foreach (DomZdravlja domZdr in Podaci.lstDomoviZdravlja)
                        if (domZdr.Id == idDZ)//ako je pronadjen dom zdr na osnovu id
                            dzLekara = domZdr;
                }



                if (upisIzmena == "upis")
                {


                    //public Adresa(int id, string ulica, int broj, string grad, string drzava)
                    int idAdresa = Podaci.generisiId_Adresa();
                    Adresa adresa = new Adresa(idAdresa, ulica, broj, grad, drzava);
                    Podaci.lstAdresa.Add(adresa);

                    Lekar l = new Lekar(ime, prezime, jmbg, email, adresa, polPacijent, lozinka, dzLekara);
                    Podaci.lstLekari.Add(l);

                }

                if (upisIzmena == "izmena")
                {
                    DialogResult = true;

                    for (int i = 0; i < Podaci.lstLekari.Count; i++)
                    {
                        if (Podaci.lstLekari.ElementAt(i).Jmbg.Equals(jmbg))
                        {
                            Podaci.lstLekari.ElementAt(i).Ime = ime;
                            Podaci.lstLekari.ElementAt(i).Prezime = prezime;
                            Podaci.lstLekari.ElementAt(i).Email = email;

                            Podaci.lstLekari.ElementAt(i).Adresa.Ulica = ulica;
                            Podaci.lstLekari.ElementAt(i).Adresa.Broj = broj;
                            Podaci.lstLekari.ElementAt(i).Adresa.Grad = grad;
                            Podaci.lstLekari.ElementAt(i).Adresa.Drzava = drzava;

                            Podaci.lstLekari.ElementAt(i).Pol = polPacijent;
                            Podaci.lstLekari.ElementAt(i).Lozinka = lozinka;

                            Podaci.lstLekari.ElementAt(i).DomZdravlja = dzLekara;




                        }
                    }



                }

                this.Close();
            }
            else
            {
                MessageBox.Show(validno(), "Greska prilikom upisa");
            }


        }
    }
}
