﻿using SF35_2018_POP2021.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF35_2018_POP2021.gui
{
    /// <summary>
    /// Interaction logic for FrmPrikazTermina.xaml
    /// </summary>
    public partial class FrmPrikazTermina : Window
    {
        public static ObservableCollection<Termin> lstTerminiDatum = new ObservableCollection<Termin>();//sadrzi termine za odredjeni datum

        Lekar lekar = null;
        Pacijent pacijent = null;
        public FrmPrikazTermina(Lekar lekar, Pacijent pacijent)
        {
            InitializeComponent();
            this.lekar = lekar;
            this.pacijent = pacijent;

            if (lekar != null)
                lbTerminiLekara.Content = "Termini lekara: " + lekar.Ime + " " + lekar.Prezime;

            for (int i = 8; i <= 19; i++)
                cbVreme.Items.Add(i.ToString());   //dodaje vremena za termine u combo box
            cbVreme.SelectedIndex = 0;

            if (pacijent != null)//onemoguciti azuriranje termina, prijavljen pacijent
            {
                btnZakaziTermin.Visibility = Visibility.Visible;
                lbVreme.Visibility = Visibility.Hidden;
                cbVreme.Visibility = Visibility.Hidden;
                lbH.Visibility = Visibility.Hidden;
                btnObrisiTermin.Visibility = Visibility.Hidden;
                btnDodaj.Visibility = Visibility.Hidden;

            }
            else
                btnZakaziTermin.Visibility = Visibility.Hidden;



            for (int i = 0; i < Podaci.lstLekari.Count; i++)//inicijalno prikaze sve termine lekara, moze se podesiti selekcijom iz date pickera
            {
                if (Podaci.lstLekari.ElementAt(i).Jmbg.Equals(lekar.Jmbg))
                {
                    dgPrikazTermina.ItemsSource = Podaci.lstLekari.ElementAt(i).LstTermini;//dg prikaz se inicijalizuje svim terminima lekara
                }
            }
        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnObrisiTermin_Click(object sender, RoutedEventArgs e)
        {
            if (dgPrikazTermina.SelectedIndex > -1)
            {
                Termin termin = (Termin)dgPrikazTermina.SelectedItem;//preuzima se objekat Termin iz selektovanog reda
                for (int i = 0; i < Podaci.lstLekari.Count; i++)//pretraga kroz listu lekara
                {
                    if (Podaci.lstLekari.ElementAt(i).Jmbg.Equals(lekar.Jmbg))//ako je pronadjen lekar po jmbg
                    {
                        int indeksBrisanje = -1;
                        for (int j = 0; j < Podaci.lstLekari.ElementAt(i).LstTermini.Count; j++)
                        {
                            if (Podaci.lstLekari.ElementAt(i).LstTermini.ElementAt(j).Id == termin.Id) //pronadjen termin za brisanje, po ID
                                indeksBrisanje = j;//zapamcen indeks za brisanje iz liste
                        }

                        int indeksBrisanjeTerminDatum = -1; //obrisati i iz liste za datum, iz ove klase
                        for (int j = 0; j < lstTerminiDatum.Count; j++)
                            if (lstTerminiDatum.ElementAt(j).Id == termin.Id)
                                indeksBrisanjeTerminDatum = j;

                        if (indeksBrisanje > -1 && (Podaci.prijavljenAdmin != null || (Podaci.prijavljenLekar != null && termin.Status != "zakazan")))
                        {
                            Podaci.lstLekari.ElementAt(i).LstTermini.RemoveAt(indeksBrisanje);
                            Podaci.lstTermini.RemoveAt(indeksBrisanje);
                            if (lstTerminiDatum.Count > 0)//ako su prikazani svi termini iz objekta lekara, lista je prazna
                                lstTerminiDatum.RemoveAt(indeksBrisanjeTerminDatum);
                            dgPrikazTermina.Items.Refresh();
                        }
                    }
                }
            }
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            DateTime datum = new DateTime();
            if (dtDatum.SelectedDate != null)//ako je selektovan datum za dodavanje
            {
                datum = dtDatum.SelectedDate.Value.Date;
                string vreme = cbVreme.SelectedItem.ToString();

                if (lekar != null)
                {
                    for (int i = 0; i < Podaci.lstLekari.Count; i++)
                    {
                        if (Podaci.lstLekari.ElementAt(i).Jmbg.Equals(lekar.Jmbg))
                        {
                            bool postoji = false;
                            foreach (Termin termin in Podaci.lstLekari.ElementAt(i).LstTermini)
                                if (termin.Datum == datum && termin.Vreme == vreme)
                                    postoji = true;//vec postoji taj termin kod ovog lekara

                            if (postoji == false)//ako vec nije dodat taj termin
                            {
                                int idTermin = Podaci.generisiId_Termin();
                                // public Termin(int id, Lekar lekar, DateTime datum, string vreme, bool status, Pacijent pacijent)
                                //status: true-dostupan, false-nedostupan
                                Termin noviTermin = new Termin(idTermin, lekar, datum, vreme, "slobodan", null);//null za pacijenta koji jos nije dodat, tj nije zakazan
                                Podaci.lstLekari.ElementAt(i).LstTermini.Add(noviTermin);
                                Podaci.lstTermini.Add(noviTermin);
                                lstTerminiDatum.Add(noviTermin);
                                //dodat u listu termina lekara, u listu svih termina i u listu ove klase za taj selektovani datum
                                    //lista iz ove klase -obrisani svi elementi svaki put kad se promeni datum

                                dgPrikazTermina.Items.Refresh();
                            }
                        }
                    }
                }
            }
        }

        private void dtDatum_SelectedDateChanged(object sender, SelectionChangedEventArgs e) //na promenu selekcije datuma prikazati u datagrid termine za taj datum lekara
        {
            lstTerminiDatum.Clear();//obrisani termini, dodati samo termine za taj datum u prikaz data grid
            DateTime datumSelekcija = dtDatum.SelectedDate.Value.Date;//selektovan datum
            for (int i = 0; i < Podaci.lstLekari.Count; i++)
            {
                if (Podaci.lstLekari.ElementAt(i).Jmbg.Equals(lekar.Jmbg))
                {
                    foreach (Termin t in Podaci.lstLekari.ElementAt(i).LstTermini)//izlistani svi termini lekara na osnovu jmbg pronadjenog
                        if (t.Datum == datumSelekcija)
                            lstTerminiDatum.Add(t);

                    dgPrikazTermina.ItemsSource = lstTerminiDatum;
                }
            }
        }

        private void btnPrikaziSve_Click(object sender, RoutedEventArgs e)//isto sto i u inicijalizaciji prikazni za sve datume
        {
            lstTerminiDatum.Clear();
            for (int i = 0; i < Podaci.lstLekari.Count; i++)
            {
                if (Podaci.lstLekari.ElementAt(i).Jmbg.Equals(lekar.Jmbg))
                {
                    dgPrikazTermina.ItemsSource = Podaci.lstLekari.ElementAt(i).LstTermini;
                }
            }
        }

        private void btnZakaziTermin_Click(object sender, RoutedEventArgs e)
        {
            if (dgPrikazTermina.SelectedIndex > -1)//mora biti selektovan neki od termina
            {
                Termin termin = (Termin)dgPrikazTermina.SelectedItem;//preuzima se objekat Termin iz selektovanog reda
                for (int i = 0; i < Podaci.lstLekari.Count; i++)//pretraga kroz listu lekara
                {
                    if (Podaci.lstLekari.ElementAt(i).Jmbg.Equals(lekar.Jmbg))//ako je pronadjen lekar po jmbg
                    { 
                        for (int j = 0; j < Podaci.lstLekari.ElementAt(i).LstTermini.Count; j++)
                        {
                            if (Podaci.lstLekari.ElementAt(i).LstTermini.ElementAt(j).Id == termin.Id)
                            {
                                //pronadjen termin lekara, setovati na zakazan i dodeliti objekat pacijenta koji se prijavio,
                                    //zatim osveziti prikaz datagrid
                                Podaci.lstLekari.ElementAt(i).LstTermini.ElementAt(j).Status = "zakazan";
                                Podaci.lstLekari.ElementAt(i).LstTermini.ElementAt(j).Pacijent = pacijent;
                                dgPrikazTermina.Items.Refresh();
                            }
                        }


                    }
                }
            }
        }
    }
}
