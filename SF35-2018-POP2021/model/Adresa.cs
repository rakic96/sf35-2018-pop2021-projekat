﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF35_2018_POP2021.model
{
    public class Adresa
    {
        int id;
        string ulica;
        int broj;
        string grad;
        string drzava;
        bool obrisan;

        public Adresa(int id, string ulica, int broj, string grad, string drzava)
        {
            this.Id = id;
            this.Ulica = ulica;
            this.Broj = broj;
            this.Grad = grad;
            this.Drzava = drzava;
            Obrisan = false;
        }

        public int Id { get => id; set => id = value; }
        public string Ulica { get => ulica; set => ulica = value; }
        public string Grad { get => grad; set => grad = value; }
        public string Drzava { get => drzava; set => drzava = value; }
        public bool Obrisan { get => obrisan; set => obrisan = value; }
        public int Broj { get => broj; set => broj = value; }

        public override string ToString()
        {
            return ulica + " " + broj + ", " + grad + ", " + drzava;
        }


    }
}
