﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SF35_2018_POP2021.model
{
    class Podaci
    {

        public static string odabran_prikaz="domovi_zdravlja";
        public static Korisnik prijavljenKorisnik = null;
        public static Pacijent prijavljenPacijent = null;
        public static Lekar prijavljenLekar = null;
        public static Administrator prijavljenAdmin = null;


        //staticna lista, moze joj se pristupiti: Podaci.lstDomoviZdravlja
        //nema potrebe za objektom, jedinstvena u celom projektu
        public static ObservableCollection<DomZdravlja> lstDomoviZdravlja = new ObservableCollection<DomZdravlja>();
        public static ObservableCollection<Adresa> lstAdresa = new ObservableCollection<Adresa>();
        //public static ObservableCollection<Korisnik> lstKorisnici = new ObservableCollection<Korisnik>();

        public static ObservableCollection<Administrator> lstAdmini = new ObservableCollection<Administrator>();
        public static ObservableCollection<Pacijent> lstPacijenti = new ObservableCollection<Pacijent>();
        public static ObservableCollection<Lekar> lstLekari = new ObservableCollection<Lekar>();

        public static ObservableCollection<Termin> lstTermini = new ObservableCollection<Termin>();
        public static ObservableCollection<Terapija> lstTerapija = new ObservableCollection<Terapija>();


        public static int generisiId_DomZdravlja() {


            int max = 0;
            foreach (DomZdravlja dz in lstDomoviZdravlja)
                if (dz.Id > max)
                    max = dz.Id;

            return ++max;
        }

        public static int generisiId_Adresa()
        {


            int max = 0;
            foreach (Adresa a in lstAdresa)
                if (a.Id > max)
                    max = a.Id;

            return ++max;
        }

        public static int generisiId_Termin()
        {


            int max = 0;
            foreach (Termin t in lstTermini)
                if (t.Id > max)
                    max = t.Id;

            return ++max;
        }
        public static int generisiId_Terapija()
        {


            int max = 0;
            foreach (Terapija t in lstTerapija)
                if (t.Id > max)
                    max = t.Id;

            return ++max;
        }


        public static void inicijalizujTestPodatke() {

            lstAdresa.Clear();
            lstDomoviZdravlja.Clear();
            //lstKorisnici.Clear();

            //    public DomZdravlja(int id, string naziv, Adresa adresa)
            // public Adresa(int id, string ulica, string grad, string drzava)

            int idAdresa1 = 1;
            string ulica = "Dunavska 12";
            string grad = "Novi Sad";
            string drzava = "Srbija";
            Adresa adresa1 = new Adresa(idAdresa1, ulica, 33, grad, drzava);

            Adresa adresa2 = new Adresa(2, "Bul.Oslobodjenja ", 22, "Novi Sad", "Srbija");

            
           // public DomZdravlja(int id, string naziv, Adresa adresa)

           
            int idDomZdravlja = 1;
            string naziv = "Dom zdravlja 1";
            DomZdravlja dz1 = new DomZdravlja(idDomZdravlja, naziv, adresa1);

            DomZdravlja dz2 = new DomZdravlja(2, "Dom zdravlja 2", adresa2);

            lstAdresa.Add(adresa1);
            lstAdresa.Add(adresa2);
            lstDomoviZdravlja.Add(dz1);
            lstDomoviZdravlja.Add(dz2);





            //string ime, string prezime, string jmbg, string email, Adresa adresa, Pol pol, string lozinka
            Adresa adresaAdmina = new Adresa(3, "Fruskogoska", 44, "Novi Sad", "Srbija");
            lstAdresa.Add(adresaAdmina);
            Administrator admin1 = new Administrator("Pera", "Peric", "123", "@peraperic", adresaAdmina, Pol.MUSKI, "123");
            lstAdmini.Add(admin1);


            //Lekar(string ime, string prezime, string jmbg, string email, Adresa adresa, Pol pol, string lozinka, DomZdravlja domZdravlja
            Adresa adresaLekara = new Adresa(4, "Bul Evrope", 123, "Novi Sad", "Srbija");
            lstAdresa.Add(adresaLekara);
            Lekar lekar = new Lekar("Jovan", "Jovanovic", "1", "@jovan", adresaLekara, Pol.MUSKI, "1", dz1);
            lstLekari.Add(lekar);

            ///public Pacijent(string ime, string prezime, string jmbg, string email, Adresa adresa, Pol pol, string lozinka)
            //: base(ime, prezime, jmbg, email, adresa, pol, lozinka)
            Adresa adresaPacijenta = new Adresa(6, "Sekspiova", 55, "Novi Sad", "Srbija");
            lstAdresa.Add(adresaPacijenta);
            
            Pacijent pacijent = new Pacijent("Marko", "Markovic", "2", "@amarko", adresaPacijenta, Pol.MUSKI, "2");
            lstPacijenti.Add(pacijent);



        }


        public static void inicijalizujPrikaz(DataGrid dataGrid)
        {

            if (Podaci.odabran_prikaz == "dom_zdravlja")
            {
                dataGrid.ItemsSource = Podaci.lstDomoviZdravlja;
            }
        }

        /*
        public static ObservableCollection<Korisnik> filtrirajKorisnike(string tipKorisnika)
        {

            ObservableCollection<Korisnik> lstFiltrirano = new ObservableCollection<Korisnik>();
            foreach(Korisnik k in lstKorisnici)
            {
                if (tipKorisnika == "administratori" && k is Administrator)
                    lstFiltrirano.Add(k);
                if (tipKorisnika == "lekari" && k is Lekar)
                    lstFiltrirano.Add(k);
                if (tipKorisnika == "pacijenti" && k is Pacijent)
                    lstFiltrirano.Add(k);
            }
            return lstFiltrirano;
        }
        */

        //po zatvaranju forme na Closing event upisati u fajlove sve liste
        public static void azurirajFajlove()
        {

            // public Adresa(int id, string ulica, int broj, string grad, string drzava)
            // public Termin(int id, Lekar lekar, DateTime datum, string vreme, string status, Pacijent pacijent) kod lekara samo
            // public Terapija(int id, string opis, Lekar lekar)

            // public Lekar(string ime, string prezime, string jmbg, string email, Adresa adresa, Pol pol, string lozinka, DomZdravlja domZdravlja)
            // public Pacijent(string ime, string prezime, string jmbg, string email, Adresa adresa, Pol pol, string lozinka)
            // public Administrator(string ime, string prezime, string jmbg, string email, Adresa adresa, Pol pol, string lozinka)
            // public DomZdravlja(int id, string naziv, Adresa adresa)


            StreamWriter sw = new StreamWriter("../../fajlovi/domovi_zdravlja.txt");
            foreach (DomZdravlja dz in lstDomoviZdravlja)
            {
                sw.WriteLine(dz.Id + "," + dz.Naziv + "," + dz.Adresa.Id);

            }
            sw.Close();

            sw = new StreamWriter("../../fajlovi/adrese.txt");
            foreach (Adresa a in lstAdresa)
            {
                sw.WriteLine(a.Id + "," + a.Ulica + "," + a.Broj + "," + a.Grad + "," + a.Drzava);

            }
            sw.Close();

            sw = new StreamWriter("../../fajlovi/termini.txt");
            foreach (Termin t in lstTermini)
            {
                string tekst = t.Id + "," + t.Lekar.Jmbg + "," + t.Datum + "," + t.Vreme + "," + t.Status + ",";
                if (t.Pacijent == null)
                    tekst += "null";
                else
                    tekst += t.Pacijent.Jmbg;
                sw.WriteLine(tekst);

            }
            sw.Close();

            sw = new StreamWriter("../../fajlovi/terapije.txt");
            foreach (Terapija t in lstTerapija)
            {
                sw.WriteLine(t.Id + "," + t.Opis + "," + t.Lekar.Jmbg + "," + t.vratiJmbgPacijenta());

            }
            sw.Close();

            sw = new StreamWriter("../../fajlovi/lekari.txt");
            foreach (Lekar l in lstLekari)
            {
                sw.WriteLine(l.Ime + "," + l.Prezime + "," + l.Jmbg + "," + l.Email + "," + l.Adresa.Id + "," + l.Pol + "," + l.Lozinka + "," + l.DomZdravlja.Id);

            }
            sw.Close();

            sw = new StreamWriter("../../fajlovi/pacijenti.txt");
            foreach (Pacijent p in lstPacijenti)
            {
                sw.WriteLine(p.Ime + "," + p.Prezime + "," + p.Jmbg + "," + p.Email + "," + p.Adresa.Id + "," + p.Pol + "," + p.Lozinka);

            }
            sw.Close();

            sw = new StreamWriter("../../fajlovi/admini.txt");
            foreach (Administrator a in lstAdmini)
            {
                sw.WriteLine(a.Ime + "," + a.Prezime + "," + a.Jmbg + "," + a.Email + "," + a.Adresa.Id + "," + a.Pol + "," + a.Lozinka);

            }
            sw.Close();




        }

        public static void ucitajFajlove()
        {




            // public Adresa(int id, string ulica, int broj, string grad, string drzava)
            StreamReader sr = new StreamReader("../../fajlovi/adrese.txt");
            string ucitanRed = sr.ReadLine();
            lstAdresa.Clear();
            while (ucitanRed != null)
            {
                string[] podaci = ucitanRed.Split(',');
                int id = Convert.ToInt32(podaci[0]);
                string ulica = podaci[1];
                int broj = Convert.ToInt32(podaci[2]);
                string grad = podaci[3];
                string drzava = podaci[4];

                Adresa a = new Adresa(id, ulica, broj, grad, drzava);
                lstAdresa.Add(a);
                ucitanRed = sr.ReadLine();


            }
           

            // public DomZdravlja(int id, string naziv, Adresa adresa)
            sr = new StreamReader("../../fajlovi/domovi_zdravlja.txt");
            ucitanRed = sr.ReadLine();
            lstDomoviZdravlja.Clear();
            while (ucitanRed != null)
            {
                string[] podaci = ucitanRed.Split(',');
                int id = Convert.ToInt32(podaci[0]);
                string naziv = podaci[1];
                int idAdresa = Convert.ToInt32(podaci[2]);

                Adresa adresa = null;
                foreach (Adresa a in lstAdresa)
                    if (a.Id == idAdresa)
                        adresa = a;

                DomZdravlja dz = new DomZdravlja(id, naziv, adresa);
                lstDomoviZdravlja.Add(dz);

                ucitanRed = sr.ReadLine();

            }


            // public Lekar(string ime, string prezime, string jmbg, string email, Adresa adresa, Pol pol, string lozinka, DomZdravlja domZdravlja)
            sr = new StreamReader("../../fajlovi/lekari.txt");
            ucitanRed = sr.ReadLine();
            lstLekari.Clear();
            while (ucitanRed != null)
            {
                string[] podaci = ucitanRed.Split(',');
                string ime = podaci[0];
                string prezime = podaci[1];
                string jmbg = podaci[2];
                string email = podaci[3];

                int idAdresa = Convert.ToInt32(podaci[4]);
                Adresa adresa = null;
                foreach (Adresa a in lstAdresa)
                    if (a.Id == idAdresa)
                        adresa = a;

                Pol pol = Pol.MUSKI;
                string ucitanPol = podaci[5];
                if (ucitanPol == "ZENSKI")
                    pol = Pol.ZENSKI;

                string lozinka = podaci[6];

                int idDZ = Convert.ToInt32(podaci[7]);
                DomZdravlja domZdr = null;
                foreach (DomZdravlja dz in lstDomoviZdravlja)
                    if (dz.Id == idDZ)
                        domZdr = dz;

                Lekar lekar = new Lekar(ime, prezime, jmbg, email, adresa, pol, lozinka, domZdr);
                lstLekari.Add(lekar);

                ucitanRed = sr.ReadLine();

            }


            // public Pacijent(string ime, string prezime, string jmbg, string email, Adresa adresa, Pol pol, string lozinka)
            sr = new StreamReader("../../fajlovi/pacijenti.txt");
            ucitanRed = sr.ReadLine();
            lstPacijenti.Clear();
            while (ucitanRed != null)
            {
                string[] podaci = ucitanRed.Split(',');
                string ime = podaci[0];
                string prezime = podaci[1];
                string jmbg = podaci[2];
                string email = podaci[3];

                int idAdresa = Convert.ToInt32(podaci[4]);
                Adresa adresa = null;
                foreach (Adresa a in lstAdresa)
                    if (a.Id == idAdresa)
                        adresa = a;

                Pol pol = Pol.MUSKI;
                string ucitanPol = podaci[5];
                if (ucitanPol == "ZENSKI")
                    pol = Pol.ZENSKI;

                string lozinka = podaci[6];


                Pacijent pacijent = new Pacijent(ime, prezime, jmbg, email, adresa, pol, lozinka);
                lstPacijenti.Add(pacijent);

                ucitanRed = sr.ReadLine();

            }


            // public Administrator(string ime, string prezime, string jmbg, string email, Adresa adresa, Pol pol, string lozinka)
            sr = new StreamReader("../../fajlovi/admini.txt");
            ucitanRed = sr.ReadLine();
            lstAdmini.Clear();
            while (ucitanRed != null)
            {
                string[] podaci = ucitanRed.Split(',');
                string ime = podaci[0];
                string prezime = podaci[1];
                string jmbg = podaci[2];
                string email = podaci[3];

                int idAdresa = Convert.ToInt32(podaci[4]);
                Adresa adresa = null;
                foreach (Adresa a in lstAdresa)
                    if (a.Id == idAdresa)
                        adresa = a;

                Pol pol = Pol.MUSKI;
                string ucitanPol = podaci[5];
                if (ucitanPol == "ZENSKI")
                    pol = Pol.ZENSKI;

                string lozinka = podaci[6];


                Administrator admin = new Administrator(ime, prezime, jmbg, email, adresa, pol, lozinka);
                lstAdmini.Add(admin);

                ucitanRed = sr.ReadLine();

            }



            // public Termin(int id, Lekar lekar, DateTime datum, string vreme, string status, Pacijent pacijent) kod lekara samo
            sr = new StreamReader("../../fajlovi/termini.txt");
            ucitanRed = sr.ReadLine();
            lstTermini.Clear();
            while (ucitanRed != null)
            {
                string[] podaci = ucitanRed.Split(',');
                int id = Convert.ToInt32(podaci[0]);

                Lekar lekar = null;
                string jmbgLekar = podaci[1];
                foreach (Lekar l in lstLekari)
                    if (l.Jmbg == jmbgLekar)
                        lekar = l;

                string datumUcitan = podaci[2];
                //DateTime datum = DateTime.Parse(datumUcitan);
                DateTime datum = new DateTime();
                if(DateTime.TryParse(datumUcitan, out datum))
                    datum = DateTime.Parse(datumUcitan);

                string vreme = podaci[3];
                string status = podaci[4];

                Pacijent pacijent = null;
                string jmbgPacijent = podaci[5];
                foreach (Pacijent p in lstPacijenti)
                    if (p.Jmbg == jmbgPacijent)
                        pacijent = p;

                Termin termin = new Termin(id, lekar, datum, vreme, status, pacijent);
                lstTermini.Add(termin);

                ucitanRed = sr.ReadLine();

            }

            foreach (Termin t in lstTermini)
            {
                for (int i = 0; i < lstLekari.Count; i++)
                {
                    if (lstLekari.ElementAt(i).Jmbg == t.Lekar.Jmbg)
                        lstLekari.ElementAt(i).LstTermini.Add(t);
                }
                for (int i = 0; i < lstPacijenti.Count; i++)
                {
                    if (t.Pacijent != null)
                        if (lstPacijenti.ElementAt(i).Jmbg == t.Pacijent.Jmbg)
                            lstPacijenti.ElementAt(i).LstTermini.Add(t);
                }

            }




            // public Terapija(int id, string opis, Lekar lekar)
            sr = new StreamReader("../../fajlovi/terapije.txt");
            ucitanRed = sr.ReadLine();
            lstTerapija.Clear();
            while (ucitanRed != null)
            {
                string[] podaci = ucitanRed.Split(',');
                int id = Convert.ToInt32(podaci[0]);

                string opis = podaci[1];

                Lekar lekar = null;
                string jmbgLekar = podaci[2];
                foreach (Lekar l in lstLekari)
                    if (l.Jmbg == jmbgLekar)
                        lekar = l;

                Pacijent pacijent = null;
                string jmbgPacijent = podaci[3];
                foreach (Pacijent p in lstPacijenti)
                    if (p.Jmbg == jmbgPacijent)
                        pacijent = p;

                Terapija terapija = new Terapija(id, opis, lekar, pacijent);
                lstTerapija.Add(terapija);

                ucitanRed = sr.ReadLine();

            }


            foreach (Terapija t in lstTerapija)
            {
                for (int i = 0; i < lstPacijenti.Count; i++)
                {
                    if (lstPacijenti.ElementAt(i).Jmbg == t.vratiJmbgPacijenta())
                        lstPacijenti.ElementAt(i).LstTerapije.Add(t);
                }

            }


        }




    }
}
