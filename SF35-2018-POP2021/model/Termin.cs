﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF35_2018_POP2021.model
{
    public class Termin
    {
        int id;
        Lekar lekar;
        DateTime datum;
        string status;
        Pacijent pacijent;
        bool obrisan;
        string vreme;

        public Termin(int id, Lekar lekar, DateTime datum, string vreme, string status, Pacijent pacijent)
        {
            this.Id = id;
            this.Lekar = lekar;
            this.Datum = datum;
            this.Vreme = vreme;
            this.Status = status;
            this.Pacijent = pacijent;
            this.obrisan = false;
        }

        public int Id { get => id; set => id = value; }
        public DateTime Datum { get => datum; set => datum = value; }
        public string Vreme { get => vreme; set => vreme = value; }
        public string Status { get => status; set => status = value; }
        //public bool Obrisan { get => obrisan; set => obrisan = value; }

        internal Lekar Lekar { get => lekar; set => lekar = value; }
        internal Pacijent Pacijent { get => pacijent; set => pacijent = value; }

        public void obrisiTermin()
        {
            this.obrisan = true;
        }
    }
}
