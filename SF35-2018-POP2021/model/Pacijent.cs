﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF35_2018_POP2021.model
{
    public class Pacijent: Korisnik
    {
        List<Terapija> lstTerapije = new List<Terapija>();
        List<Termin> lstTermini = new List<Termin>();
        //za liste ne treba konstruktor, inicijalno prazne, tj ne treba im dodeliti pocetnu vrednost

        public Pacijent(string ime, string prezime, string jmbg, string email, Adresa adresa, Pol pol, string lozinka)
            : base(ime, prezime, jmbg, email, adresa, pol, lozinka)
        {
        }

        internal List<Terapija> LstTerapije { get => lstTerapije; set => lstTerapije = value; }
        internal List<Termin> LstTermini { get => lstTermini; set => lstTermini = value; }
    }
}
