﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF35_2018_POP2021.model
{
    public enum Pol { MUSKI, ZENSKI};
    public class Korisnik
    {
        string ime;
        string prezime;
        string jmbg;
        string email;
        Adresa adresa;
        Pol pol;
        string lozinka;
        bool obrisan;

        string ulica;
        int broj;
        string grad;
        string drzava;

        public Korisnik(string ime, string prezime, string jmbg, string email, Adresa adresa, Pol pol, string lozinka)
        {
            this.Ime = ime;
            this.Prezime = prezime;
            this.Jmbg = jmbg;
            this.Email = email;
            this.Adresa = adresa;
            this.Pol = pol;
            this.Lozinka = lozinka;
            this.obrisan = false;

            this.Ulica = adresa.Ulica;
            this.Broj = adresa.Broj;
            this.Grad = adresa.Grad;
            this.Drzava = adresa.Drzava;
        }

        public string Ime { get => ime; set => ime = value; }
        public string Prezime { get => prezime; set => prezime = value; }
        public string Jmbg { get => jmbg; set => jmbg = value; }
        public string Email { get => email; set => email = value; }
        public Pol Pol { get => pol; set => pol = value; }
        public string Lozinka { get => lozinka; set => lozinka = value; }
        //public bool Obrisan { get => obrisan; set => obrisan = value; }
        internal Adresa Adresa { get => adresa; set => adresa = value; }

        public string Ulica { get => ulica; set => ulica = value; }
        public int Broj { get => broj; set => broj = value; }
        public string Grad { get => grad; set => grad = value; }
        public string Drzava { get => drzava; set => drzava = value; }

        public void obrisiKorisnika()
        {

            this.obrisan = true;
        }
    }
}
