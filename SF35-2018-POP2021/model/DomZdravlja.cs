﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF35_2018_POP2021.model
{
    public class DomZdravlja
    {
        int id;
        string naziv;
        Adresa adresa;
        public bool obrisan;

        string ulica;
        int broj;
        string grad;
        string drzava;

        public DomZdravlja(int id, string naziv, Adresa adresa)
        {
            this.Id = id;
            this.Naziv = naziv;
            this.Adresa = adresa;
            //this.Obrisan = false;
            this.obrisan = false;

            this.Ulica = adresa.Ulica;
            this.Broj = adresa.Broj;
            this.Grad = adresa.Grad;
            this.Drzava = adresa.Drzava;
        }

        public int Id { get => id; set => id = value; }
        public string Naziv { get => naziv; set => naziv = value; }
        //public bool Obrisan { get => obrisan; set => obrisan = value; }
        public string Ulica { get => ulica; set => ulica = value; }
        internal Adresa Adresa { get => adresa; set => adresa = value; }
        public int Broj { get => broj; set => broj = value; }
        public string Grad { get => grad; set => grad = value; }
        public string Drzava { get => drzava; set => drzava = value; }

        public override string ToString()
        {
            return id + " " + naziv + ", adresa: " + adresa.ToString();
        }

        public void obrisiDZ()
        {
            this.obrisan = true;
        }
        public bool daLiJeObrisan()
        {
            return this.obrisan;
        }
    }
}
