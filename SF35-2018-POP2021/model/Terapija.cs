﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF35_2018_POP2021.model
{
    class Terapija
    {
        int id;
        string opis;
        Lekar lekar;
        bool obrisan;
        Pacijent pacijent;

        public Terapija(int id, string opis, Lekar lekar, Pacijent pacijent)
        {
            this.Id = id;
            this.Opis = opis;
            this.Lekar = lekar;
            this.obrisan = false;
            this.pacijent = pacijent;
        }

        public int Id { get => id; set => id = value; }
        public string Opis { get => opis; set => opis = value; }
        //public Pacijent Pacijent { get => pacijent; set => pacijent = value; }

        //public bool Obrisan { get => obrisan; set => obrisan = value; }
        internal Lekar Lekar { get => lekar; set => lekar = value; }

        public string vratiJmbgPacijenta()
        {
            string vrati = "";
            if (pacijent != null)
                vrati = pacijent.Jmbg;
            return vrati;
        }
    }
}
