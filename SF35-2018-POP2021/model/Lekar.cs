﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF35_2018_POP2021.model
{
    public class Lekar: Korisnik
    {
        DomZdravlja domZdravlja;
        List<Termin> lstTermini = new List<Termin>();

        string nazivDomaZdravlja;
        string adresaDomaZdravlja;

        //lekar sadrzi sve sto i Korisnik, dodat DomZdravljaC:\Users\Stefan Rakic\source\repos\SF35-2018-POP2021\SF35-2018-POP2021\model\Lekar.cs
        public Lekar(string ime, string prezime, string jmbg, string email, 
            Adresa adresa, Pol pol, string lozinka, DomZdravlja domZdravlja)
            :base(ime, prezime, jmbg, email, adresa, pol, lozinka)//prosledjeni baznom konstruktoru
        {
            this.DomZdravlja = domZdravlja;
            this.NazivDomaZdravlja = domZdravlja.Naziv;
            this.adresaDomaZdravlja = domZdravlja.Ulica + " " + domZdravlja.Broj + " " + domZdravlja.Grad;
        }

        public string NazivDomaZdravlja { get => nazivDomaZdravlja; set => nazivDomaZdravlja = value; }
        public string AdresaDomaZdravlja { get => adresaDomaZdravlja; set => adresaDomaZdravlja = value; }

        internal DomZdravlja DomZdravlja { get => domZdravlja; set => domZdravlja = value; }
        internal List<Termin> LstTermini { get => lstTermini; set => lstTermini = value; }
    }
}
