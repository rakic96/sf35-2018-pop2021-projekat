﻿
using SF35_2018_POP2021.gui;
using SF35_2018_POP2021.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SF35_2018_POP2021
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public static ObservableCollection<DomZdravlja> lstDomoviZdravlja_MestoPacijenta = new ObservableCollection<DomZdravlja>();
        public static ObservableCollection<Lekar> lstLekari_MestoPacijenta = new ObservableCollection<Lekar>();

        public static ObservableCollection<DomZdravlja> lstDomoviZdravlja_Lekar = new ObservableCollection<DomZdravlja>();
        public static ObservableCollection<Pacijent> lstPacijenti_Zakazani = new ObservableCollection<Pacijent>();

        //dodat Singleton sablon, jedna instanca klase MainWinndow u celom projektu
        public static MainWindow instance = null; //inicijalno null

        public static MainWindow Instance   //static odredjuje da ce biti jedinstveno na nivou celog projekta
        {
            get
            {
                if (instance == null) //ako ne postoji napraviti
                {
                    instance = new MainWindow();
                }
                return instance;
            }
        }
       

        //string odabranPrikaz = "dom_zdravlja";//podrazumevano domovi zdravlja
        public MainWindow()
        {
            InitializeComponent();

            //Podaci.inicijalizujTestPodatke(); 

            Podaci.ucitajFajlove();
            dgPrikaz.ItemsSource = Podaci.lstDomoviZdravlja;


            cbOdabranPrikaz.Items.Add("Domovi zdravlja");
            cbOdabranPrikaz.SelectedIndex = 0;

            btnUpis.Visibility = Visibility.Hidden;
            btnIzmena.Visibility = Visibility.Hidden;
            btnBrisanje.Visibility = Visibility.Hidden;

            btnPrikaz.Visibility = Visibility.Hidden;
            btnRegistracijaPacijenta.Visibility = Visibility.Visible;
            btnPrikazTermina.Visibility = Visibility.Hidden;
            btnOdjava.Visibility = Visibility.Hidden;

            btnPrikazTerapija.Visibility = Visibility.Hidden;//22.01.
            btnPrikazTermina.Visibility = Visibility.Hidden;


        }
        

        private void btnUpis_Click(object sender, RoutedEventArgs e)
        {
            //if (dgPrikaz.SelectedIndex >= 0)
            //{
                if (Podaci.odabran_prikaz == "domovi_zdravlja")
                {
                    FrmUpis_DomZdravlja frmUpis = new FrmUpis_DomZdravlja("upis", null);
                    frmUpis.Show();
                }
                if (Podaci.odabran_prikaz == "pacijenti")
                {
                    FrmRegistracijaPacijenta frmUpis = new FrmRegistracijaPacijenta("upis", null);
                    frmUpis.ShowDialog();

                    // if (frmUpis.DialogResult.HasValue && frmUpis.DialogResult.Value)
                    // dgPrikaz.Items.Refresh();
                }
                if (Podaci.odabran_prikaz == "lekari")
                {
                    FrmRegistracijaLekara frmUpis = new FrmRegistracijaLekara("upis", null);
                    frmUpis.ShowDialog();

                    //if (frmUpis.DialogResult.HasValue && frmUpis.DialogResult.Value)
                    //dgPrikaz.Items.Refresh();
                }
           // }

        }

        private void btnIzmena_Click(object sender, RoutedEventArgs e)
        {
            if (dgPrikaz.SelectedIndex >= 0)
            {
                if (Podaci.odabran_prikaz == "domovi_zdravlja")
                {
                    DomZdravlja domZdravlja = (DomZdravlja)dgPrikaz.SelectedItem;
                    FrmUpis_DomZdravlja frmUpis = new FrmUpis_DomZdravlja("izmena", domZdravlja);
                    frmUpis.ShowDialog();

                    if (frmUpis.DialogResult.HasValue && frmUpis.DialogResult.Value) //ako je na otvorenoj formi kliknuto potvrdno za izmenu
                        dgPrikaz.Items.Refresh();

                }

                if (Podaci.odabran_prikaz == "pacijenti")
                {
                    Pacijent pacijent = (Pacijent)dgPrikaz.SelectedItem;
                    FrmRegistracijaPacijenta frmUpis = new FrmRegistracijaPacijenta("izmena", pacijent);
                    frmUpis.ShowDialog();

                    if (frmUpis.DialogResult.HasValue && frmUpis.DialogResult.Value)
                        dgPrikaz.Items.Refresh();

                }

                if (Podaci.odabran_prikaz == "lekari")
                {
                    Lekar lekar = (Lekar)dgPrikaz.SelectedItem;
                    FrmRegistracijaLekara frmUpis = new FrmRegistracijaLekara("izmena", lekar);
                    frmUpis.ShowDialog();

                    if (frmUpis.DialogResult.HasValue && frmUpis.DialogResult.Value)
                        dgPrikaz.Items.Refresh();

                }
            }
        }

        private void dgPrikaz_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(cbOdabranPrikaz.Text=="Domovi zdravlja")
            {

            }
        }

        private void btnBrisanje_Click(object sender, RoutedEventArgs e)
        {
            if (dgPrikaz.SelectedIndex >= 0)
            {
                if (Podaci.odabran_prikaz == "domovi_zdravlja")
                {
                    DomZdravlja domZdravlja = (DomZdravlja)dgPrikaz.SelectedItem;
                    FrmPotrvrdaBrisanja frm = new FrmPotrvrdaBrisanja(domZdravlja);
                    frm.ShowDialog();

                    if (frm.DialogResult.HasValue && frm.DialogResult.Value)
                    {
                        int indeksBrisanje = -1;
                        for (int i = 0; i < Podaci.lstDomoviZdravlja.Count; i++) {  //iteracije kroz listu od 0 do broja elemenata liste
                            if (Podaci.lstDomoviZdravlja.ElementAt(i).Id == domZdravlja.Id)  //da li element na poziciji i ima ID jednak ID selektovanog DZ iz Datagrid
                                indeksBrisanje = i;
                        }
                        if(indeksBrisanje>-1)
                            Podaci.lstDomoviZdravlja.RemoveAt(indeksBrisanje);
                        dgPrikaz.Items.Refresh();
                    }
                }
                if (Podaci.odabran_prikaz == "pacijenti")
                {
                    Pacijent pacijent = (Pacijent)dgPrikaz.SelectedItem;
                    FrmPotrvrdaBrisanja frm = new FrmPotrvrdaBrisanja(pacijent);
                    frm.ShowDialog();

                    if (frm.DialogResult.HasValue && frm.DialogResult.Value)
                    {
                        int indeksBrisanje = -1;
                        for (int i = 0; i < Podaci.lstPacijenti.Count; i++)
                        {  
                            if (Podaci.lstPacijenti.ElementAt(i).Jmbg == pacijent.Jmbg)  
                                indeksBrisanje = i;
                        }
                        if (indeksBrisanje > -1)
                            Podaci.lstPacijenti.RemoveAt(indeksBrisanje);

                        dgPrikaz.Items.Refresh();
                    }
                }
                if (Podaci.odabran_prikaz == "lekari")
                {
                    Lekar lekar = (Lekar)dgPrikaz.SelectedItem;
                    FrmPotrvrdaBrisanja frm = new FrmPotrvrdaBrisanja(lekar);
                    frm.ShowDialog();

                    if (frm.DialogResult.HasValue && frm.DialogResult.Value)
                    {
                        if (frm.DialogResult.HasValue && frm.DialogResult.Value)
                        {
                            int indeksBrisanje = -1;
                            for (int i = 0; i < Podaci.lstLekari.Count; i++)
                            {
                                if (Podaci.lstLekari.ElementAt(i).Jmbg == lekar.Jmbg)
                                    indeksBrisanje = i;
                            }
                            if (indeksBrisanje > -1)
                                Podaci.lstLekari.RemoveAt(indeksBrisanje);

                            dgPrikaz.Items.Refresh();
                        }
                    }
                }
            }

        }



        private void click_prijavaAdmina(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Prijava admina!");
            FrmPrijavaKorisnika frm = new FrmPrijavaKorisnika();
            frm.ShowDialog();

            if (frm.DialogResult.HasValue && frm.DialogResult.Value)
            {
                if (Podaci.prijavljenKorisnik is Administrator)
                {
                    cbOdabranPrikaz.Items.Clear();
                    cbOdabranPrikaz.Items.Add("Domovi zdravlja");
                    cbOdabranPrikaz.Items.Add("Lekari");
                    cbOdabranPrikaz.Items.Add("Pacijenti");
                    cbOdabranPrikaz.SelectedIndex = 0;

                    btnUpis.Visibility = Visibility.Visible;
                    btnIzmena.Visibility = Visibility.Visible;
                    btnBrisanje.Visibility = Visibility.Visible;
                    btnPrikaz.Visibility = Visibility.Visible;
                    btnRegistracijaPacijenta.Visibility = Visibility.Hidden;
                    btnOdjava.Visibility = Visibility.Visible;
                }
            }

        }
        private void click_prijavaPacijenta(object sender, RoutedEventArgs e)
        {
            FrmPrijavaKorisnika frm = new FrmPrijavaKorisnika();
            frm.ShowDialog();

            if (frm.DialogResult.HasValue && frm.DialogResult.Value)
            {
                if (Podaci.prijavljenKorisnik is Pacijent)
                {
                    cbOdabranPrikaz.Items.Clear();
                    cbOdabranPrikaz.Items.Add("Domovi zdravlja");
                    cbOdabranPrikaz.Items.Add("Lekari");
                    cbOdabranPrikaz.SelectedIndex = 0;

                    btnPrikaz.Visibility = Visibility.Visible;
                    btnRegistracijaPacijenta.Visibility = Visibility.Hidden;
                    btnOdjava.Visibility = Visibility.Visible;

                    btnPrikazTerapija.Visibility = Visibility.Visible;//22.01.

                    inicijalizujListe_MestoPacijenta();
                }
            }
        }
        void inicijalizujListe_MestoPacijenta()
        {

            lstDomoviZdravlja_MestoPacijenta.Clear();
            lstLekari_MestoPacijenta.Clear();

            if (Podaci.prijavljenPacijent != null)
            {

                foreach (DomZdravlja dz in Podaci.lstDomoviZdravlja)
                    if (dz.Grad == Podaci.prijavljenPacijent.Grad)
                        lstDomoviZdravlja_MestoPacijenta.Add(dz);

                foreach (Lekar lekar in Podaci.lstLekari)
                    foreach (DomZdravlja dz in lstDomoviZdravlja_MestoPacijenta)
                        if (lekar.DomZdravlja.Id == dz.Id)
                            lstLekari_MestoPacijenta.Add(lekar);   //u listu za pregled u profilu pacijenta prikazati lekare zaposlene u domovima zdravlja iz grada pacijenta
                                                                   //if (lekar.Grad == Podaci.prijavljenPacijent.Grad)
                                                                   //lstLekari_MestoPacijenta.Add(lekar);

                dgPrikaz.ItemsSource = lstDomoviZdravlja_MestoPacijenta;
            }
        }

        private void click_prijavaLekara(object sender, RoutedEventArgs e)
        {
            FrmPrijavaKorisnika frm = new FrmPrijavaKorisnika();
            frm.ShowDialog();

            if (frm.DialogResult.HasValue && frm.DialogResult.Value)
            {
                if (Podaci.prijavljenKorisnik is Lekar)
                {
                    cbOdabranPrikaz.Items.Clear();
                    cbOdabranPrikaz.Items.Add("Domovi zdravlja");
                    cbOdabranPrikaz.Items.Add("Pacijenti");
                    cbOdabranPrikaz.SelectedIndex = 1;

                    btnPrikaz.Visibility = Visibility.Visible;
                    btnRegistracijaPacijenta.Visibility = Visibility.Hidden;
                    btnOdjava.Visibility = Visibility.Visible;
                    btnPrikazTermina.Visibility = Visibility.Visible;

                    //btnPrikazTerapija.Visibility = Visibility.Visible;
                    btnPrikazTermina.Visibility = Visibility.Visible;//22.01.

                    inicijalizujListe_LekarPrijavljen();

                }
            }
        }

        void inicijalizujListe_LekarPrijavljen() {

            lstDomoviZdravlja_Lekar.Clear();
            lstPacijenti_Zakazani.Clear();//obrisati sve, ponovnom prijavom bi dupliralo podatke

            if (Podaci.prijavljenLekar != null)
            {
                foreach (DomZdravlja dz in Podaci.lstDomoviZdravlja)
                    if (dz.Id == Podaci.prijavljenLekar.DomZdravlja.Id)
                        lstDomoviZdravlja_Lekar.Add(dz);
                foreach (Termin termin in Podaci.prijavljenLekar.LstTermini)//kroz listu termina lekara
                    if (termin.Pacijent != null) //!=null ako je zakazan, prilikom zakazivanja setuje se objekat prijavljenog pacijenta
                        lstPacijenti_Zakazani.Add(termin.Pacijent);

                Podaci.odabran_prikaz = "pacijenti";
                dgPrikaz.ItemsSource = lstPacijenti_Zakazani;

            }
        }

        private void btnOdjava_Click(object sender, RoutedEventArgs e)
        {
            Podaci.prijavljenKorisnik = null;
            cbOdabranPrikaz.Items.Clear();
            cbOdabranPrikaz.Items.Add("Domovi zdravlja");
            cbOdabranPrikaz.SelectedIndex = 0;
            dgPrikaz.ItemsSource = Podaci.lstDomoviZdravlja;
            dgPrikaz.Items.Refresh();

            btnUpis.Visibility = Visibility.Hidden;
            btnIzmena.Visibility = Visibility.Hidden;
            btnBrisanje.Visibility = Visibility.Hidden;
            btnPrikaz.Visibility = Visibility.Hidden;
            btnRegistracijaPacijenta.Visibility = Visibility.Visible;
            btnOdjava.Visibility = Visibility.Hidden;

            btnPrikazTerapija.Visibility = Visibility.Hidden;//22.01.
            btnPrikazTermina.Visibility = Visibility.Hidden;

            Podaci.prijavljenLekar = null;
            Podaci.prijavljenPacijent = null;
            Podaci.prijavljenAdmin = null;
            lstDomoviZdravlja_MestoPacijenta.Clear();
            lstLekari_MestoPacijenta.Clear();
        }

        private void cbOdabranPrikaz_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //MessageBox.Show(cbOdabranPrikaz.SelectedItem.ToString());
            if (cbOdabranPrikaz.HasItems) //ako ima elemenata za selekciju
            {
                if (cbOdabranPrikaz.SelectedItem.ToString() == "Domovi zdravlja")
                {
                    if (Podaci.prijavljenLekar == null)
                        btnPrikazTermina.Visibility = Visibility.Hidden;

                    dgPrikaz.ItemsSource = Podaci.lstDomoviZdravlja;
                    if (Podaci.prijavljenPacijent != null) //dg prikaz prikazuje listu domova zdravlja iz mesta pacijetna
                        dgPrikaz.ItemsSource = lstDomoviZdravlja_MestoPacijenta;
                    if (Podaci.prijavljenLekar != null)
                        dgPrikaz.ItemsSource = lstDomoviZdravlja_Lekar;
                    dgPrikaz.Items.Refresh();
                    Podaci.odabran_prikaz = "domovi_zdravlja";
                }
                if (cbOdabranPrikaz.SelectedItem.ToString() == "Lekari")
                {
                    btnPrikazTermina.Visibility = Visibility.Visible;

                    dgPrikaz.ItemsSource = Podaci.lstLekari;
                    if (Podaci.prijavljenPacijent != null)//dg prikaz prikazuje listu lekara iz mesta pacijetna
                        dgPrikaz.ItemsSource = lstLekari_MestoPacijenta;

                    if (Podaci.prijavljenAdmin != null || Podaci.prijavljenPacijent != null) //22.01.
                        btnPrikazTermina.Visibility = Visibility.Visible;

                    dgPrikaz.Items.Refresh();
                    Podaci.odabran_prikaz = "lekari";
                }
                if (cbOdabranPrikaz.SelectedItem.ToString() == "Pacijenti")
                {
                    if (Podaci.prijavljenLekar == null)
                        btnPrikazTermina.Visibility = Visibility.Hidden;

                    dgPrikaz.ItemsSource = Podaci.lstPacijenti;
                    if (Podaci.prijavljenLekar != null)
                        dgPrikaz.ItemsSource = lstPacijenti_Zakazani;

                    if (Podaci.prijavljenAdmin != null || Podaci.prijavljenLekar != null) //22.01.
                        btnPrikazTerapija.Visibility = Visibility.Visible;

                    dgPrikaz.Items.Refresh();
                    Podaci.odabran_prikaz = "pacijenti";
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void btnPrikaz_Click(object sender, RoutedEventArgs e) //prikaz profila prijavljenog lekara ili pacijenta
        {
            
            // MessageBox.Show("Test");
            //Object obj = (Object)Podaci.prijavljenKorisnik;
            if (Podaci.prijavljenLekar!=null)
            {
                Lekar lekarPrikaz = Podaci.prijavljenLekar;
                FrmRegistracijaLekara frm = new FrmRegistracijaLekara("izmena", lekarPrikaz);
                frm.ShowDialog();
            }
            if (Podaci.prijavljenPacijent!=null)
            {
                Pacijent pacijentPrikaz = Podaci.prijavljenPacijent;
                FrmRegistracijaPacijenta frm = new FrmRegistracijaPacijenta("izmena", pacijentPrikaz);
                frm.ShowDialog();
            }
        }

        private void btnRegistracijaPacijenta_Click(object sender, RoutedEventArgs e)
        {
            FrmRegistracijaPacijenta frm = new FrmRegistracijaPacijenta("upis", null);
            frm.ShowDialog();
        }

        private void btnPrikazTermina_Click(object sender, RoutedEventArgs e)
        {
            if (dgPrikaz.SelectedIndex > -1 && Podaci.odabran_prikaz == "lekari" && Podaci.prijavljenAdmin != null)//prijavljen admin
            {

                Lekar lekar = (Lekar)dgPrikaz.SelectedItem;
                FrmPrikazTermina frm = new FrmPrikazTermina(lekar, null);
                frm.ShowDialog();
            }
            else if (Podaci.prijavljenLekar != null)//prijavljen lekar
            {

                FrmPrikazTermina frm = new FrmPrikazTermina(Podaci.prijavljenLekar, null);
                frm.ShowDialog();

            }
            else if (Podaci.prijavljenPacijent != null && dgPrikaz.SelectedIndex > -1 && Podaci.odabran_prikaz == "lekari")//pacijent prijavljen
            {
                Lekar lekar = (Lekar)dgPrikaz.SelectedItem;
                FrmPrikazTermina frm = new FrmPrikazTermina(lekar, Podaci.prijavljenPacijent);
                frm.ShowDialog();
            }
        }

        private void btnPrikazTerapija_Click(object sender, RoutedEventArgs e)
        {
            if (Podaci.odabran_prikaz == "pacijenti" && dgPrikaz.SelectedIndex > -1)
            {

                Pacijent pacijent = (Pacijent)dgPrikaz.SelectedItem;

                if (Podaci.prijavljenLekar != null)//prijavljen lekar, prikazati formu terapija s mogucnostima azuriranja
                {
                    FrmPrikazTerapija frm = new FrmPrikazTerapija(pacijent, Podaci.prijavljenLekar);
                    frm.ShowDialog();

                }
                if (Podaci.prijavljenAdmin != null)
                {
                    FrmPrikazTerapija frm = new FrmPrikazTerapija(pacijent, null);
                    frm.ShowDialog();
                }

            }

            if (Podaci.prijavljenPacijent != null || Podaci.prijavljenAdmin != null)//pacijent moze da pregleda karton bez selekcije iz datagrid
            {
                FrmPrikazTerapija frm = new FrmPrikazTerapija(Podaci.prijavljenPacijent, null);//mogucnost pregleda kartona, bez azuriranja za prijavljenog pacijenta
                frm.ShowDialog();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Podaci.azurirajFajlove();
        }

        private void btnPretrazi_Click(object sender, RoutedEventArgs e)
        {
            string tekstPretraga = tbPretraga.Text;

            if (Podaci.odabran_prikaz == "domovi_zdravlja")
            {

                ObservableCollection<DomZdravlja> lstDomoviZdr = new ObservableCollection<DomZdravlja>();
                if (tekstPretraga != "")
                    foreach (DomZdravlja dz in Podaci.lstDomoviZdravlja)
                    {
                        if (dz.Naziv.Contains(tekstPretraga) || dz.Adresa.Ulica.Contains(tekstPretraga)) // || 
                            lstDomoviZdr.Add(dz);
                        if (int.TryParse(tekstPretraga, out int test))
                            if (dz.Adresa.Broj == Convert.ToInt32(tekstPretraga))
                                lstDomoviZdr.Add(dz);

                    }

                if (lstDomoviZdr.Count > 0)
                {
                    dgPrikaz.ItemsSource = lstDomoviZdr;
                    dgPrikaz.Items.Refresh();
                }

            }
            if (Podaci.odabran_prikaz == "lekari")
            {

                ObservableCollection<Lekar> lstLekari = new ObservableCollection<Lekar>();
                if (tekstPretraga != "")
                    foreach (Lekar l in Podaci.lstLekari)
                    {
                        if (l.Ime.Contains(tekstPretraga) || l.Prezime.Contains(tekstPretraga) || l.Email.Contains(tekstPretraga) || l.Adresa.Ulica.Contains(tekstPretraga))//|| 
                            lstLekari.Add(l);
                        if (int.TryParse(tekstPretraga, out int test))
                            if (l.Adresa.Broj == Convert.ToInt32(tekstPretraga))
                                lstLekari.Add(l);
                    }

                if (lstLekari.Count > 0)
                {
                    dgPrikaz.ItemsSource = lstLekari;
                    dgPrikaz.Items.Refresh();
                }

            }
            if (Podaci.odabran_prikaz == "pacijenti")
            {

                ObservableCollection<Pacijent> lstPacijenti = new ObservableCollection<Pacijent>();
                if (tekstPretraga != "")
                    foreach (Pacijent p in Podaci.lstPacijenti)
                    {
                        if (p.Ime.Contains(tekstPretraga) || p.Prezime.Contains(tekstPretraga) || p.Email.Contains(tekstPretraga) || p.Adresa.Ulica.Contains(tekstPretraga))//|| 
                            lstPacijenti.Add(p);
                        if (int.TryParse(tekstPretraga, out int test))
                            if (p.Adresa.Broj == Convert.ToInt32(tekstPretraga))
                                lstPacijenti.Add(p);
                    }

                if (lstPacijenti.Count > 0)
                {
                    dgPrikaz.ItemsSource = lstPacijenti;
                    dgPrikaz.Items.Refresh();
                }
            }
        }

        private void btnPrikaziSve_Click(object sender, RoutedEventArgs e)
        {
            tbPretraga.Text = "";

            if (Podaci.odabran_prikaz == "domovi_zdravlja")
            {
                dgPrikaz.ItemsSource = Podaci.lstDomoviZdravlja;
                dgPrikaz.Items.Refresh();
            }
            if (Podaci.odabran_prikaz == "lekari")
            {
                dgPrikaz.ItemsSource = Podaci.lstLekari;
                dgPrikaz.Items.Refresh();
            }
            if (Podaci.odabran_prikaz == "pacijenti")
            {
                dgPrikaz.ItemsSource = Podaci.lstPacijenti;
                dgPrikaz.Items.Refresh();
            }
        }
    }
}
